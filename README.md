# 使用NPM

```PowerShell
$ npm i -g xiaolu-p
$ npm i --save xiaolu-p
```



# 项目引入

```PowerShell
import lu from 'xiaolu-p'
Vue.use(lu)
```





# 修改xiaolu-p内置配置

```PowerShell
// main.js
import lu from 'xiaolu-p'
Vue.use(lu)

// 调用setConfig方法，方法内部会进行对象属性深度合并，可以放心嵌套配置
// 需要在Vue.use(lu)之后执行

uni.$lu.setConfig({
	// 修改$lu.config对象的属性
	config: {
		// 修改默认单位为rpx，相当于执行 uni.$lu.config.unit = 'rpx'
		unit: 'rpx'
	},
	// 修改$lu.props对象的属性
	props: {
		// 修改radio组件的size参数的默认值，相当于执行 uni.$lu.props.radio.size = 30
		radio: {
			size: 15
		}
		// 其他组件属性配置
		// ......
	}
})
```



# 请求工具库的配置请参考开源工具luch-request文档

```PowerShell
https://www.quanzhan.co/luch-request/guide/3.x/
```

