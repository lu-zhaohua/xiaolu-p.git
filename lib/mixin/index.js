export default {
    onLoad() {
        this.$lu.getRect = this.$uGetRect
    },
    created() {
        this.$lu.getRect = this.$uGetRect
    },
    methods: {
        // 查询节点信息
        $uGetRect(selector, all) {
            return new Promise((resolve) => {
                uni.createSelectorQuery().in(this)[all ? 'selectAll' : 'select'](selector).boundingClientRect((rect) => {
                    if (all && Array.isArray(rect) && rect.length) {
                        resolve(rect)
                    }
                    if (!all && rect) {
                        resolve(rect)
                    }
                }).exec()
            })
        },
    },
};
