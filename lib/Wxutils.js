export default class Wx {
	/**
	 * 
	 * @param {String} AppID - appid
	 * @param {String} AppSecret - appsecret
	 */
	constructor(opction = {}) {
		setTimeout(() => {
			this.AppID = uni.$lu?.config?.wx?.AppID;
			this.AppSecret = uni.$lu?.config?.wx?.AppSecret;
		}, 0)
	}

	/**
	 * @description 微信登录
	 * @returns code
	 */
	getcode() {
		return new Promise(resolve => {
			wx.login({
				success: function(loginRes) {
					if (loginRes?.code) return resolve(loginRes.code)
				}
			});
		})
	}

	/**
	 * @description 获取openid和session_key
	 * @returns oppenid、session_key
	 */
	async getopenid() {
		const code = await this.getcode()
		return new Promise(resolve => {
			wx.request({
				url: `https://api.weixin.qq.com/sns/jscode2session?appid=${this.AppID}&secret=${this.AppSecret}&js_code=${code}&grant_type=authorization_code`,
				method: 'GET',
				success: (res) => {
					if (res?.data) return resolve(res?.data)
				}
			})
		})
	}

	/**
	 * @description 获取access_token
	 * @returns {String} access_token
	 */
	async getaccess_token() {
		return new Promise(resolve => {
			wx.request({
				url: `https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${this.AppID}&secret=${this.AppSecret}`,
				method: 'GET',
				success: (res) => {
					if (res?.data) return resolve(res?.data)
				}
			})
		})
	}


	/**
	 * @description 获取用户手机号码
	 * @param {String} phonecode 
	 * @returns {String} phoneNumber
	 */
	async getphone(phonecode) {
		if (!phonecode) new Error('code不能为空')
		const {
			access_token
		} = await this.getaccess_token()
		return new Promise(resolve => {
			wx.request({
				url: `https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=${access_token}`,
				data: {
					code: phonecode
				},
				method: 'post',
				success: (res) => {
					if (res?.data?.phone_info) return resolve(res.data.phone_info)
				}
			})
		})
	}


	/**
	 * @description 获取用户资料
	 * @returns {Promise}
	 */
	async getuserinfo() {
		return new Promise((resolve, reject) => {
			wx.getUserProfile({
				desc: '用于完善会员信息',
				success: (res) => {
					resolve(res)
				},
				fail: (error) => {
					reject(error)
				}
			})
		})
	}

	/**
	 * @description 生物识别
	 * @param {String} challenge - 挑战因子
	 */
	biometricIdentification(challenge = "20205424") {
		// #ifdef MP-WEIXIN
		return new Promise(resolve => {
			wx.checkIsSupportSoterAuthentication({
				success(res) {
					const status = {
						fingerPrint: '请使用指纹解锁',
						facial: '请使用脸部解锁'
					}
					wx.startSoterAuthentication({
						requestAuthModes: res.supportMode,
						challenge: challenge, //挑战因子
						authContent: status['requestAuthModes'],
						success(res) {
							resolve({
								resultJSON: res?.resultJSON,
								resultJSONSignature: res?.resultJSONSignature
							})
						}
					})
				}
			})
		})
		// #endif
	}

	/**
	 * @description 生物验证
	 * @returns {Boolean}
	 */
	async biologicalVerification() {
		// #ifdef MP-WEIXIN
		const {
			openid
		} = await this.getopenid()
		const {
			access_token
		} = await this.getaccess_token()
		const {
			resultJSON,
			resultJSONSignature
		} = await this.biometricIdentification()
		return new Promise(resolve => {
			wx.request({
				url: `http://api.weixin.qq.com/cgi-bin/soter/verify_signature?access_token=${access_token}`,
				data: {
					openid: openid,
					json_string: resultJSON,
					json_signature: resultJSONSignature
				},
				method: 'post',
				success: (res) => resolve(res?.data?.is_ok)
			})
		})
		// #endif
	}

}