// lu-ui 相关的配置
import color from './color.js'
export default {
	// lu-ui单位
	unit: 'px',
	// 颜色
	color: color,
	// 微信类
	wx: {},
	// 网络请求
	http: {},
	// 是否路由跳转时是否登录
	checkLogin: {
		// 是否检查跳转登录
		isCheck: false,
		// 跳转登录的路由
		checkBackUrl: '/',
		// 需要检查登录的缓存对象字段
		checkText: 'token'
	}
}