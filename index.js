// lu-ui相关的配置
import config from "./lib/config";
// 正则相关
import test from "./lib/test";
// 工具类
import util from "./lib/util";
// 运算的工具
import digit from "./lib/digit";
// 相关的颜色
import color from "./lib/config/color.js";
// 微信工具类
import Wx from "./lib/Wxutils.js";
// 图片上传类
import Upload from "./lib/upload.js";
// 网络请求
import Request from "./lib/luch-request";
// 节流
import throttle from "./lib/throttle.js";
// 防抖
import debounce from "./lib/debounce.js";
// 颜色转换
import colorGradient from "./lib/colorGradient.js";
// 全局混入
import mixin from "./lib/mixin/index";

const $utils = {
	wx: new Wx(),
	upload: new Upload(),
	http: new Request(),
	mixin,
	test,
	digit,
	color,
	config,
	throttle,
	debounce,
	colorGradient,
	...util,
};

uni.$lu = $utils;

export default {
	install(Vue) {
		Vue.prototype.$lu = $utils;
		const requireComponents = require.context("./components", true, /\.vue$/);
		requireComponents.keys().forEach((cptPath) => {
			// 组件名
			const cptName = cptPath.match(/\.\/(.+?)\//)[1];
			// 注册组件
			Vue.component(cptName, requireComponents(cptPath).default);
			Vue.mixin(mixin);
		});
	},
};
