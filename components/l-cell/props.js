/** @format */

export default {
	props: {
		// 图标类
		icon: {
			type: String,
			default: undefined,
		},
		// 图标大小
		iconSize: {
			type: [String, Number],
			default: 16,
		},
		// 图标颜色
		iconColor: {
			type: String,
			default: undefined,
		},
		// 右侧图标类
		righticon: {
			type: String,
			default: "jiantou-01",
		},
		// 右侧图标大小
		righticonSize: {
			type: [String, Number],
			default: 16,
		},
		// 右侧图标颜色
		righticonColor: {
			type: String,
			default: undefined,
		},
		// 是否展示右侧箭头并开启点击反馈
		isLink: {
			type: Boolean,
			default: true,
		},
		// 标题
		title: {
			type: String,
			default: undefined,
		},
		// 右侧标题
		value: {
			type: String,
			default: undefined,
		},
	},
};
