export default {
	props: {
		// 主题颜色
		type: {
			type: String,
			default: "default",

		},
		// 字体大小
		size: {
			type: [String, Number],
			default: "16",
		},
		// 是否圆角
		shape: {
			type: String,
			default: "defalut",
		},
		// 大小尺寸
		size: {
			type: String,
			default: "normal",
		},
		// 是否禁用
		disabled: {
			type: Boolean,
			default: false,
		},
		// 是否镂空
		plain: {
			type: Boolean,
			default: false,
		},
	},
};