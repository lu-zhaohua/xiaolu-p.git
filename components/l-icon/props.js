export default {
	props: {
		// icon名称
		name: {
			type: String,
			default: ''
		},
		// 主题色
		type: {
			type: String,
			default: ''
		},
		// 图标颜色
		color: {
			type: String,
			default: ''
		},
		// 背景颜色
		bg: {
			type: String,
			default: ''
		},
		// 标签位置
		labelpops: {
			type: String,
			default: ''
		},
		// 标签文字
		label: {
			type: String,
			default: ''
		},
		// 文字颜色
		textcolor: {
			type: String,
			default: ''
		},
		// 自定义样式 对象
		activeStyle: {
			type: [String, Object],
			default: ''
		},
		// 图标大小
		size: {
			type: [String, Number],
			default: 25
		},
		// 是否粗体
		bold: {
			type: Boolean,
			default: false
		},
		// 字体大小
		fontSize: {
			type: [String, Number],
			default: 16
		}
	},
}