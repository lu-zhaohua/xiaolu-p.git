/** @format */

export default {
	props: {
		// 通过v-model双向绑定的值
		value: {
			type: Boolean,
			default: false,
		},
		// 打开时的背景颜色
		activeColor: {
			type: String,
			default: "#3c9cff",
		},
		// 关闭时的背景颜色
		inactiveColor: {
			type: String,
			default: "#f3f4f6",
		},
		// 是否禁用
		disabled: {
			type: Boolean,
			default: false,
		},
		// 尺寸
		size: {
			type: String,
			default: "normal",
		},
		// 加载中
		loading: {
			type: Boolean,
			default: false,
		},
	},
	model: {
		prop: "value",
		event: "change",
	},
};
