export default {
	props: {
		// 是否支持解码
		decode: {
			type: Boolean,
			default: false
		},
		// 是否连续空格
		space: {
			type: Boolean,
			default: false
		},
		// 主题颜色
		type: {
			type: String,
			default: 'info'
		},
		// 是否粗体
		bold: {
			type: Boolean,
			default: false
		},
		// 字体大小
		size: {
			type: [String, Number],
			default: '16'
		},
		// 显示行数，超出省略...
		lines: {
			type: [String, Number],
			default: '1'
		},
		// 前缀图标
		prefixIcon: {
			type: String,
			default: undefined
		},
		// 后缀图标
		suffixIcon: {
			type: String,
			default: undefined
		},
		// 图标颜色
		iconColor: {
			type: String,
			default: undefined
		},
		// 图标主题颜色
		iconType: {
			type: String,
			default: undefined
		},
		// 图标大小
		iconSize: {
			type: [String, Number],
			default: 16
		},
		// 图标是否粗体
		iconbold: {
			type: Boolean,
			default: false
		},
		// 微信开放能力
		openType: {
			type: String,
			default: undefined
		}
	}
}